import React from 'react';
import TestUtils from 'react-addons-test-utils';
import HomeComponent from '../HomeComponent';

test('display greeting', () => {
  const homeComponent = TestUtils.renderIntoDocument(<HomeComponent />);

  const h1 = TestUtils.findRenderedDOMComponentWithTag(
    homeComponent, 'h1',
  );

  expect(h1.textContent).toBe('Hello, !');
});
