module RbCAW
  class ConduitLazyModel
    attr_reader :phid
    def initialize(phid, conduit: nil)
      @fetched = false

      @phid = phid
      @conduit = conduit
    end

    def check_fetched
      unless @fetched
        fetch
      end
    end

    def manual_init
      raise NoMethodError, 'Initialization method not defined'
    end

    def fetch
      raise NoMethodError, 'Fetch method not defined'
    end

    def to_s
      raise NoMethodError, 'Stringify method not defined'
    end
  end
end