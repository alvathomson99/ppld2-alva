require_relative '../models/conduit_lazy_model'

module RbCAW
  class ProjectLazyModel < ConduitLazyModel
    def manual_init(id, name, description, icon, color, members, slug,
                    date_created, date_modified, start_date, end_date, is_sprint)
      @id = id
      @name = name
      @description = description
      @icon = icon
      @color = color
      @members = members
      @slug = slug
      @date_created = date_created
      @date_modified = date_modified
      @start_date = start_date
      @end_date = end_date
      @is_sprint = is_sprint

      @fetched = true
    end

    def fetch
      project = @conduit.project.get(phid: @phid)

      manual_init(
          project.id, project.name, project.description, project.icon,
          project.color, project.members, project.slug, project.date_created,
          project.date_modified, project.start_date, project.end_date, project.is_sprint
      )
    end

    def id
      check_fetched
      return @id
    end

    def name
      check_fetched
      return @name
    end

    def description
      check_fetched
      return @description
    end

    def icon
      check_fetched
      return @icon
    end

    def color
      check_fetched
      return @color
    end

    def members
      check_fetched
      return @members
    end

    def slug
      check_fetched
      return @slug
    end

    def date_created
      check_fetched
      return @date_created
    end

    def date_modified
      check_fetched
      return @date_modified
    end

    def start_date
      check_fetched
      return @start_date
    end

    def end_date
      check_fetched
      return @end_date
    end

    def is_sprint
      check_fetched
      return @is_sprint
    end
  end
end