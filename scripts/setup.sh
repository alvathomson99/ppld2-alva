#!/bin/bash

sudo apt-get -qq update

# mongodb
echo "Install MongoDB"
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
echo 'deb http://downloads-distro.mongodb.org/repo/ubuntu-upstart dist 10gen' | sudo tee /etc/apt/sources.list.d/mongodb.list
sudo apt-get -qq update
sudo apt-get install -y mongodb
sudo service mongodb start

# nodejs
echo "Install Node.js"
sudo apt-get install curl
curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -
sudo apt-get install -y nodejs

echo "Install Gems"
gem install bundler
bundle install

echo "Install Node Packages"
npm install

cp env.sample .env
