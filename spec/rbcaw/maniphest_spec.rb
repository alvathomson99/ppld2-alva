require 'rbcaw/conduit'

RSpec.describe RbCAW::Conduit, '.maniphest#search' do
  context 'with proper connection to the host' do
    before do
      VCR.insert_cassette 'maniphest_search', :record => :new_episodes
    end

    it 'should retrieve tasks\' info properly' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])
      search_result = conduit.maniphest.search(constraints: {'phids': ['PHID-TASK-jlukmmdkfivzryxhnxab']})

      expect(search_result).to_not eql(nil)
      expect(search_result.results).to_not eql(nil)
      expect(search_result.results.length).to eql(1)

      expect(search_result.results[0]).to respond_to(:phid)
      expect(search_result.results[0]).to respond_to(:id)
      expect(search_result.results[0]).to respond_to(:name)
      expect(search_result.results[0]).to respond_to(:author)
      expect(search_result.results[0]).to respond_to(:owner)
      expect(search_result.results[0]).to respond_to(:status)
      expect(search_result.results[0]).to respond_to(:priority)
      expect(search_result.results[0]).to respond_to(:projects)
      expect(search_result.results[0]).to respond_to(:date_created)
      expect(search_result.results[0]).to respond_to(:date_modified)
      expect(search_result.results[0]).to respond_to(:sprint_info)

      expect(search_result.results[0].phid).to eql('PHID-TASK-jlukmmdkfivzryxhnxab')
      expect(search_result.results[0].name).to eql('Unit Test Task #1')
      expect(search_result.results[0].owner.username).to eql('unit_test')
      expect(search_result.results[0].sprint_info.point).to eql(10)
      expect(search_result.results[0].sprint_info.is_closed).to eql(false)
    end

    after do
      VCR.eject_cassette
    end
  end

  context 'while disconnected from the Phabricator host' do
    before do
      stub_request(:any, /#{CONDUIT['host']}/).to_timeout
    end

    it 'should not be able to retrieve info and raised a TimeoutError' do
      conduit = RbCAW::Conduit.new(CONDUIT['host'], CONDUIT['api_token'])

      expect { conduit.maniphest.search }.to raise_error(Timeout::Error)
    end
  end
end